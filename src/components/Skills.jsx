import React from "react";

const Skills = () => {
  const daftarKemampuan = [
    {
      logo: "devicon-html5-plain-wordmark",
      size: "90px",
    },
    {
      logo: "devicon-php-plain",
      size: "90px",
    },
    {
      logo: "devicon-java-plain-wordmark",
      size: "90px",
    },
    {
      logo: "devicon-javascript-plain",
      size: "90px",
    },
    {
      logo: "devicon-visualbasic-plain",
      size: "90px",
    },
    {
      logo: "devicon-css3-plain-wordmark",
      size: "90px",
    },
    {
      logo: "devicon-typescript-plain",
      size: "90px",
    },
    {
      logo: "devicon-go-original-wordmark",
      size: "90px",
    },
    {
      logo: "devicon-firebase-plain-wordmark",
      size: "90px",
    },
    {
      logo: "devicon-mongodb-plain-wordmark",
      size: "90px",
    },
    {
      logo: "devicon-postgresql-plain-wordmark",
      size: "90px",
    },
    {
      logo: "devicon-mysql-plain-wordmark",
      size: "90px",
    },
    {
      logo: "devicon-microsoftsqlserver-plain-wordmark",
      size: "90px",
    },
    {
      logo: "devicon-spring-original-wordmark",
      size: "90px",
    },
    {
      logo: "devicon-bootstrap-plain-wordmark",
      size: "90px",
    },
    {
      logo: "devicon-tailwindcss-plain-wordmark",
      size: "90px",
    },
    {
      logo: "devicon-nodejs-plain-wordmark",
      size: "90px",
    },
    {
      logo: "devicon-laravel-original-wordmark",
      size: "90px",
    },
    {
      logo: "devicon-codeigniter-plain-wordmark",
      size: "90px",
    },
    {
      logo: "devicon-angular-plain-wordmark",
      size: "90px",
    },
    {
      logo: "devicon-react-original-wordmark",
      size: "90px",
    },
    {
      logo: "devicon-postman-plain-wordmark",
      size: "90px",
    },
    {
      logo: "devicon-redis-plain-wordmark",
      size: "90px",
    },
    {
      logo: "devicon-git-plain-wordmark",
      size: "90px",
    }
  ];

  const skills = [
    {
      logo: "logo-html5",
      level: "Advance",
      count: 86,
    },
    {
      logo: "logo-css3",
      level: "Expect",
      count: 90,
    },
    {
      logo: "logo-nodejs",
      level: "Beginner",
      count: 40,
    },
    {
      logo: "logo-react",
      level: "Intermediate",
      count: 80,
    }
  ];
  return (
    <section id="skills" className="py-10 bg-gray-800 relative">
      <div className="mt-8 text-gray-100 text-center">
        <h3 className="text-4xl font-semibold">
          Pernah <span className="text-cyan-600">Menggunakan</span>
        </h3>
        <div className="flex items-center justify-center mt-12 gap-10 flex-wrap">
          {/*{skills?.map((skill, i) => (*/}
          {/*  <div*/}
          {/*    key={i}*/}
          {/*    className="border-2 group border-cyan-600 relative min-w-[10rem] max-w-[16rem] bg-gray-900 p-10 rounded-xl"*/}
          {/*  >*/}
          {/*    <div*/}
          {/*      style={{*/}
          {/*        background: `conic-gradient(rgb(8,145,170) ${skill.count}%,#ddd ${skill.count}%)`,*/}
          {/*      }}*/}
          {/*      className="w-32 h-32 flex items-center justify-center rounded-full"*/}
          {/*    >*/}
          {/*      <div className="text-6xl w-28 h-28 bg-gray-900 rounded-full flex items-center justify-center group-hover:text-cyan-600">*/}
          {/*        <ion-icon name={skill.logo}></ion-icon>*/}
          {/*      </div>*/}
          {/*    </div>*/}
          {/*    <p className="text-xl mt-3">{skill.level}</p>*/}
          {/*  </div>*/}
          {/*))}*/}

          {daftarKemampuan?.map((kemampuan) => (
              <i style={{fontSize:kemampuan.size}} className={kemampuan.logo + " hover:text-cyan-600"}></i>
          ))}

        </div>
      </div>
    </section>
  );
};

export default Skills;
