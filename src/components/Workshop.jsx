import React from "react";

const Workshop = () => {
    const daftarWorkshop = [
        {
            perusahaan: "Enigma Camp",
            posisi: "Trainee",
            durasi: "Juni 2023",
            teknologi: "JavaScript · Bootstrap (Framework) · Git · Node.js · PostgreSQL · Java · Spring Boot · AngularJS · React · REST API · Postman API",
            keterangan: "Mempelajari pengetahuan dasar bahasa program, back end, front end web dan mobile"
        }
    ];

    return(
        <section id="workshop" className="py-10 text-white">
            <div className="mt-8">
                <h3 className="text-center text-4xl font-semibold">
                    Work<span className="text-cyan-600">shop</span>
                </h3>
                <div className="m-5 justify-center flex">
                    {daftarWorkshop.map((hasil) => (
                        <div>
                            <p className="text-gray-200 font-bold">{hasil.durasi}</p>
                            <h3 className="text-2xl font-semibold">{hasil.perusahaan}</h3>
                            <p className="text-gray-200">{hasil.posisi}</p>
                            <br/>
                            <p className="text-gray-200">Teknologi : {hasil.teknologi}</p>
                            <p className="text-gray-200">{hasil.keterangan}</p>
                        </div>
                    ))}
                </div>
            </div>
        </section>
    );
}

export default Workshop;