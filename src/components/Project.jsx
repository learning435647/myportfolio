import React, {useState} from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import project1 from "../assets/images/project-1.png";
import project2 from "../assets/images/project-2.jpg";
import project3 from "../assets/images/project-3.jpg";
import project4 from "../assets/images/project-4.jpg";
import project5 from "../assets/images/project-5.png";
import undrawFeelingProud from "../assets/images/UndrawFeelingProud.png";
import project_person from "../assets/images/project_person1.png";
import "swiper/css";
import "swiper/css/pagination";
import { Pagination, Autoplay } from "swiper";
import Modal from "./Modal";

const Project = () => {

  const projects = [
      {
        namaProject: "Tokonyadia",
        perusahaan: "Enigma",
        posisi: "Back End",
          durasi: "-",
        teknologi: "Java Spring Boot, Spring Security JWT, Spring Data JPA, Spring AOP, PostgreSQL, Maven, JUnit dan Mockito",
        keterangan: "Tokonyadia adalah aplikasi e-commerce dalam bentuk Back End API"
      },
      {
        namaProject: "DDistance",
        perusahaan: "Enigma",
        posisi: "Back End, Front End Mobile",
          durasi: "3 Minggu",
        teknologi: "Java Spring Boot, Spring Security JWT, Spring Data JPA, Spring AOP, PostgreSQL, React Native, Expo",
        keterangan: "DDistance adalah aplikasi peminjaman dana dari Bank untuk toko"
      }
  ];

  return (
    <section id="projects" className="py-10 text-white">
      <div className="text-center">
        <h3 className="text-4xl font-semibold">
          Pro<span className="text-cyan-600">ject</span>
        </h3>
      </div>
      <div className="m-5 flex-wrap flex justify-center">
          {projects.map((result, i) => (
              <div className="md:w-1/3 m-2 p-8 bg-gray-800 rounded-lg">
                  <h3 className="text-2xl font-semibold">{result.namaProject}</h3>
                  <p className="text-gray-200">{result.posisi}</p>
                  <br/>
                  <p className="text-gray-200 font-bold">{result.durasi}</p>
                  <p className="text-gray-200">Teknologi : {result.teknologi}</p>
                  <p className="text-gray-200">{result.keterangan}</p>
              </div>
          ))}
      </div>
    </section>
  );
};

export default Project;
