import React from "react";
const PengalamanKerja = () => {
  const workExperiences = [
    {
      company: "PT. Absolute Connection",
      position: "Research And Development",
      duration: "Agustus 2018 - Desember 2020",
      technology: "Visual Basic ASP.NET, SQL Server",
      responsibilities: [
        "Kordinasi dengan Bisnis Analis",
        "Menerapkan fitur laporan",
        "Menerapkan fitur proses bisnis ke dalam web",
      ],
    },
    {
      company: "Wellma Consulting",
      position: "Web Programmer",
      duration: "December 2020 - January 2022",
      technology: "CodeIgniter, Bootstrap, MySQL, Firebase",
      responsibilities: [
        "Kordinasi dengan Owner dan Develop Mobile",
        "Pemeliharaan Web",
        "Mengunggah Web menjadi publik",
        "Menerapkan fitur proses bisnis ke dalam web",
      ],
    },
    {
      company: "PT. Inova Medika Solusindo",
      position: "Web Programmer",
      duration: "February 2022 - February 2023",
      technology: "YII 1, Node JS, Bootstrap, PostgreSQL",
      responsibilities: [
        "Kordinasi dengan Bisnis Analis",
        "Menerapkan fitur laporan",
        "Menerapkan fitur proses bisnis ke dalam web",
      ],
    },
  ];

  return (
    <section id="pengalamanKerja" className="py-10 text-white">
      <div className="mt-8">
        <h3 className="text-center text-4xl font-semibold">
          Pengalaman <span className="text-cyan-600">Kerja</span>
        </h3>

        <div className="justify-center flex flex-wrap">
            {workExperiences.map((experience, index) => (
                <div className="m-2 p-8 bg-gray-800 rounded-lg">
                  <h3 className="text-2xl font-semibold">{experience.company}</h3>
                  <p className="text-gray-200">{experience.position}</p>
                  <br/>
                  <p className="text-gray-200 font-bold">{experience.duration}</p>
                  <p className="text-gray-200">Teknologi : {experience.technology}</p>
                  <ul className="list-disc pl-6 mt-4">
                    {experience.responsibilities.map((responsibility, idx) => (
                        <li key={idx} className="text-gray-200">{responsibility}</li>
                    ))}
                  </ul>
              </div>
            ))}
        </div>
      </div>
    </section>
  );
};

export default PengalamanKerja;
