import React from "react";
import hero from "../assets/images/hero.png";
import Intro from  "../assets/images/Intro2.png"
import {url} from "tailwindcss/lib/util/dataTypes";

const Hero = () => {
  const social_media = [
    {icon: "logo-instagram", url: "https://www.instagram.com/igabdulwahid"},
    // {icon: "logo-facebook", url: "#"},
    {icon: "logo-linkedin", url: "https://www.linkedin.com/in/abdulwahidkhoeruddin"},
    // {icon: "logo-gitlab", url: "#"},
  ];

  const handleIconSosmed = (url) => {
    if(url != "#") window.open(url, "_blank");
  }

  return (
    <section
      id="home"
      className="min-h-screen flex py-10 md:flex-row flex-col items-center"
    >
      <div className="flex-1 flex items-center justify-center h-full">
        <img src={Intro} alt="" className="md:w-11/12 h-full object-cover" />
      </div>
      <div className="flex-1">
        <div className="md:text-left text-center">
          <h1 className="md:text-5xl text-2xl md:leading-normal leading-10 text-white font-bold">
            <span className="text-cyan-600 md:text-6xl text-5xl">
              Halo !
              <br />
            </span>
            Saya Abdul Wahid K
            <br/>
            di panggil Heru
          </h1>
          <h4 className="md:text-2xl text-lg md:leading-normal leading-5 mt-4 font-bold text-gray-600">
            Fullstack Developer
          </h4>
          <a href="./src/assets/CvByResume.io.pdf" download>
            <button className="btn-primary mt-8">Unduh CV</button>
          </a>
          <div className="mt-8 text-3xl flex items-center md:justify-start justify-center gap-5">
            {social_media?.map((result) => (
              <div
                key={result.icon}
                className="text-gray-600 hover:text-white cursor-pointer "
                onClick={() => handleIconSosmed(result.url)}
              >
                <ion-icon name={result.icon}></ion-icon>
              </div>
            ))}
          </div>
        </div>
      </div>
    </section>
  );
};

export default Hero;
