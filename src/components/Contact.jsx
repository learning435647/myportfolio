import React from "react";
import Modal from "./Modal";

const Contact = () => {
  const contact_info = [
    { logo: "mail", text: "abdulwahidkhoeruddin@gmail.com", url: "mailto:abdulwahidkhoeruddin@gmail.com" },
    { logo: "logo-whatsapp", text: "087771368866", url: "https://wa.me/087771368866" },
  ];
  const handleIcon = (url) => {
    if(url != "#") window.open(url, "_blank");
  }

  return (
    <section id="contact" className="py-10 px-3 text-white">
      <div className="mt-8">
        <h3 className="text-center text-4xl font-semibold">
          Kontak <span className="text-cyan-600">Saya</span>
        </h3>
        <div
          className="mt-8 flex md:flex-row flex-col
         gap-6 max-w-5xl bg-gray-800 md:p-6 p-2 rounded-lg mx-auto justify-center"
        >
          {/*<form className="flex flex-col flex-1 gap-5">*/}
          {/*  <input type="text" placeholder="Your Name" />*/}
          {/*  <input type="Email" placeholder="Your Email Address" />*/}
          {/*  <textarea placeholder="Your Message" rows={10}></textarea>*/}
          {/*  <button className="btn-primary w-fit">Send Message</button>*/}
          {/*</form>*/}

          <div className="flex flex-row gap-7 justify-center">
            {contact_info.map((contact, i) => (
                <a href={contact.url} target="_blank" rel="noopener noreferrer">
                  <div
                      key={i}
                      className="flex flex-col
                  text-left gap-4 flex-wrap items-center"
                  >
                    <div className="min-w-[3.5rem]  text-3xl min-h-[3.5rem] flex items-center justify-center text-white bg-cyan-600 rounded-full">
                      <ion-icon name={contact.logo}></ion-icon>
                    </div>
                    <p className="md:text-base text-sm  break-words">
                    </p>
                  </div>
                </a>
            ))}
          </div>
        </div>
      </div>
    </section>
  );
};

export default Contact;
